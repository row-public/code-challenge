import { createTheme } from "@mui/material/styles";

export const themeOptions = {
  palette: {

    background: {
      default: '#fffffe',
      paper: '#fffffe',
      offWhite: '#fffffd',
      floralWhite: '#fefbf3',
    },

    common: {
      white: '#fffffe',
      black: '#20201d',
      offWhite: '#fffffd',
      floralWhite: '#fefbf3',
    },

    text: {
      default: '#20201d',
      primary: '#20201d',
    },

    primary: {
      light: '#747370',
      main: '#494845',
      dark: '#22211e',
      contrastText: '#fffffd',
    },

    secondary: {
      light: '#ffe070',
      main: '#ffd129',
      dark: '#c7a000',
      contrastText: '#20201d',
    },

    tertiary: {
      main: '#2734b3',
      contrastText: '#fffffd',
    },

    floralWhite: {
      main: '#fefbf3',
      contrastText: '#20201d',
    },

    green: {
      main: '#4CAF50',
      contrastText: '#20201d',
    },

    orange: {
      main: '#ff7845',
      contrastText: '#20201d',
    },

    blue: {
      main: '#2184bf',
      contrastText: '#fffffd',
    },

    purple: {
      main: '#6a21bf',
      contrastText: '#fffffd',
    },

    info: {
      main: "#6b79ff",
      focus: "#99a3ff",
    },

    success: {
      main: '#4CAF50',
      focus: '#67bb6a',
    },

    warning: {
      main: '#fb8c00',
      focus: '#fc9d26',
    },

    error: {
      main: '#F44335',
      focus: '#f65f53',
    },

    grey: {
      main: '#7e7b77',
      contrastText: '#fffffd',
      100: "#f5f5f4",
      200: "#d7d6d5",
      300: "#bab9b5",
      400: "#a6a4a0",
      500: "#7e7b77",
      600: "#696763",
      700: "#4a4845",
      800: "#353331",
      900: "#151514",
    },

    yellow: {
      main: '#ffe070',
      contrastText: '#20201d',
      100: "#fff6d6",
      200: "#ffedad",
      300: "#ffe485",
      400: "#ffdc5c",
      500: "#ffd129",
      600: "#ffca0a",
      700: "#cca000",
      800: "#8f7000",
      900: "#524000",
    },

  },

  typography: {
    fontFamily: "\"Inter\", \"Helvetica\", \"Arial\", sans-serif",
  }
};

export default createTheme(themeOptions);