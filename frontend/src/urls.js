import InputIcon from '@mui/icons-material/Input';
import EmojiObjectsIcon from '@mui/icons-material/EmojiObjectsOutlined';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import DashboardIcon from '@mui/icons-material/Dashboard';
import HomeIcon from '@mui/icons-material/Home';
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
import ContactsIcon from '@mui/icons-material/Contacts';
import WorkIcon from '@mui/icons-material/Work';
import ContactPageIcon from '@mui/icons-material/ContactPage';


const userUrls = {
    home: '/account/',
    profile: 'profile',
    bankAccount: 'dwolla_profile',
    providers: 'providers',
}

const crmUrls = {
    home: '/crm/',
    leads: 'leads',
    opportunities: 'opportunities',
    contacts: 'contacts',
}

const finUrls = {
    home: '/fin/',
    invoices: 'invoices',
    customers: 'customers',
}

const urls = {
    apiSession: '/api/session/',
    apiLogout: '/api/logout/',
    apiLogin: '/api/login/',
    apiVerifyEmail: '/api/verify_email/',

    apiSignup: '/api/signup',
    apiOnboarding: '/api/onboarding',
    apiWorkspaces: '/api/workspaces',

    apiCmsPages: '/api/cms/pages',
    
    apiDwollaUserProfile: '/api/user/dwolla_profile',
    apiDwollaFundingSources: '/api/user/funding_sources',
    apiGuestDwollaUserProfile: '/api/user/guest_dwolla_profile',

    apiLeads: '/api/crm/leads',
    apiOpportunities: '/api/crm/opportunities',
    apiContacts: '/api/crm/contacts',

    apiProposals: '/api/sales-document/proposals',

    apiInvoices: '/api/billing/invoices',
    apiCustomers: '/api/billing/customers',
    apiProviders: '/api/billing/providers',
    apiBillingGuestSession: '/api/billing/guest_session/',

    root: '/',
    login: '/login/',
    home: '/home/',
    rowOnboarding: '/onboarding/',
    termsOfService: '/terms/terms-of-service/',
    privacyPolicy: '/terms/privacy-policy',

    user: userUrls,
    userHome: userUrls.home,
    userProfile: `${userUrls.home}${userUrls.profile}`,
    userBankAccount: `${userUrls.home}${userUrls.bankAccount}`,
    userProviders: `${userUrls.home}${userUrls.providers}`,

    crm: crmUrls,
    crmHome: crmUrls.home,
    crmLeads: `${crmUrls.home}${crmUrls.leads}`,
    crmOpportunities: `${crmUrls.home}${crmUrls.opportunities}`,
    crmContacts: `${crmUrls.home}${crmUrls.contacts}`,

    fin: finUrls,
    finHome: finUrls.home,
    finInvoices: `${finUrls.home}${finUrls.invoices}`,
    finCustomers: `${finUrls.home}${finUrls.customers}`,

    docsHome: '/docs/',
    docsProposals: '/sales-document/proposals',
}

export const urlMetadata = {
    [urls.home]: { icon: HomeIcon, label: "Home" },

    [urls.userProfile]: {icon: ContactPageIcon, label: "Profile"},
    [urls.userBankAccount]: {icon: AccountBalanceIcon, label: "Bank Accounts"},

    [urls.crmHome]: { icon: DashboardIcon, label: "Overview", breadcrumb: "CRM" },
    [urls.crmLeads]: { icon: InputIcon, label: "Leads" },
    [urls.crmOpportunities]: { icon: EmojiObjectsIcon, label: "Opportunities" },
    [urls.crmContacts]: { icon: ContactsIcon, label: "Contacts" },

    [urls.docsHome]: { icon: DashboardIcon, label: "Documents" },

    [urls.finHome]: { icon: DashboardIcon, label: "Overview", breadcrumb: "Accounting" },
    [urls.finInvoices]: { icon: ReceiptLongIcon, label: "Invoices" },
    [urls.finCustomers]: { icon: WorkIcon, label: "Clients" },
};

export function getUrlIcon(url) {
    return urlMetadata[url]?.icon;
}

export function getUrlLabel(url) {
    return urlMetadata[url]?.label;
}

export function getBreadcrumb(url) {
    return getUrlLabel(url);
}

export function isSubpathOf(candidate, url) {
    return candidate && url && candidate.startsWith(url);
}

export default urls;