import Box from "@mui/material/Box";
import React from "react";

import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'

import DndList from "../../components/board/DndList";

export default {
    title: "Board/DndList",
};

const Template = ({ listProps, items, renderItem, renderDrop, onChange }) => {
    return (
        <DndProvider backend={HTML5Backend}>
            <DndList
                listProps={listProps}
                items={items}
                renderItem={renderItem}
                renderDrop={renderDrop}
            />
        </DndProvider>
    );
};

const defaultArgs = {}

export const Example = Template.bind({});
Example.args = {
    ...defaultArgs,
    items: ['ONE', 'TWO', 'THREE'],
    renderItem: (item) => <Box border={1} p={5} width={100}>{item}</Box>,
    renderDrop: (dragItem) => <Box border={1} p={5} width={100}>Drop here.</Box>,
}
