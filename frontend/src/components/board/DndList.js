import { v4 as uuidv4 } from 'uuid';
import React from "react";

import { useDrag, useDrop } from 'react-dnd';
import { TransitionGroup } from 'react-transition-group';

import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";

export const DraggableTypes = {
    ITEM: 'item',
}


export function DndItem({ index, item, renderDrop, children, sx }) {
    const [{ DragItemType, DragItem, isDragging, canDrag }, drag] = useDrag(() => {
        return ({
            type: DraggableTypes.ITEM,
            item: (monitor) => {
                // Called at beginning of the drag.
                // Returning null cancels drag.
                return item;
            },
            canDrag: (monitor) => {
                return true;
            },
            end: (dragItem, monitor) => {
                // Check whether or not the drop was handled by a compatible target.
                const didDrop = monitor.didDrop();
                // dropResult is returned from the target's drop() method.
                const dropResult = monitor.getDropResult();
            },
            collect: (monitor) => {
                return {
                    DragItemType: monitor.getItemType(),
                    DragItem: monitor.getItem(),
                    isDragging: monitor.isDragging(),
                    canDrag: monitor.canDrag(),
                };
            },
        });
    }, [index]);

    const [{ isOver, canDrop, dragItem, dragItemType }, drop] = useDrop(() => {
        return ({
            accept: [DraggableTypes.ITEM],
            canDrop: (dragItem, monitor) => {
                return true;
            },
            drop: (dragItem, monitor) => {
            },
            collect: (monitor) => {
                return {
                    dragItem: monitor.getItem(),
                    dragItemType: monitor.getItemType(),
                    isOver: monitor.isOver(),
                    canDrop: monitor.canDrop(),
                };
            },
        });
    }, [index]);

    const dropArea = React.useMemo(() => renderDrop(dragItem), [dragItem]);

    return (
        <Box
            ref={(node) => drop(drag(node))}
            display="flex" flexBasis="100%" flexWrap="wrap"
            sx={{
                opacity: isDragging ? 0.5 : 1,
                ...sx
            }}
        >
            <Collapse
                in={!isDragging && isOver}
                component="div"
                sx={{ display: "flex", flexBasis: '100%' }}
            >
                {dropArea}
            </Collapse>
            {children}
        </Box>
    );
}


export default function DndList({ listProps, items, renderItem, renderDrop, onChange }) {
    let memoizedListProps = React.useMemo(() => {
        let _listProps = listProps || {};
        if (typeof _listProps === 'function') {
            _listProps = listProps();
        }
        _listProps.id = _listProps.id || uuidv4();
        return _listProps;
    }, [listProps]);

    let renderedItems = React.useMemo(() => {
        items = items || [];
        renderItem = renderItem || (item => item);
        return items.map((item, index) => [item, renderItem(item, index)]);
    }, [items, renderItem])

    return (
        <Box display="flex" flexWrap="wrap">
            {renderedItems.map(([item, child], index) => {
                if (typeof item !== 'object') {
                    item = { content: item };
                }
                item.list = memoizedListProps;
                return (
                    <DndItem
                        key={index}
                        index={index}
                        item={item}
                        renderDrop={renderDrop}
                    >
                        {child}
                    </DndItem>
                );
            })}
        </Box>
    );
}