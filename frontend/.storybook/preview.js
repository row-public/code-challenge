import RoutesDecorator from './routesDecorator';
import StylesDecorator from './stylesDecorator';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};

export const decorators = [
  RoutesDecorator,
  StylesDecorator,
]