import React from "react";
import { MemoryRouter } from "react-router-dom";

const RoutesDecorator = Story => (
  <MemoryRouter>
    <Story />
  </MemoryRouter>
);

export default RoutesDecorator;