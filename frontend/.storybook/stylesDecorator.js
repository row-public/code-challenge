import React from "react";

import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles";

import LightTheme from '../src/themes/lightTheme';

const StylesDecorator = Story => (
  <StyledEngineProvider injectFirst>
    <ThemeProvider theme={LightTheme}>
      <Story />
    </ThemeProvider>
  </StyledEngineProvider>
);

export default StylesDecorator;